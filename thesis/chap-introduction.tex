%% ------------------------------------------------------------------------- %%
\chapter{Introduction}
\label{cap:introduction}

Free/Libre/Open Source Software (FLOSS) is a term used to encompass all
software released under a license that allows its users the freedom to inspect,
use, modify and redistribute the source code~\citep{crowston2012}. Since its
inception by the Free Software (in the 1980s) and the Open-source (in the
1990s) movements, the FLOSS usage has grown both in the industry and for
personal use.  Its popularization also increased the interest of understanding
its practices of development and community organization. However, despite many
investigations about it, FLOSS remains a commonly misunderstood phenomenon,
with significant gaps between theory and practice over more than two
decades~\citep{osterlie2007}.

In the world of software engineering, one of the research opportunities which
arise from FLOSS studies is the motivations that drive individuals to
participate in these projects and what prompts them to continue
contributing~\citep{scacchi2010}. Much of this is because FLOSS projects are
commonly community-based, and the community depends on a continuous stream of
contributors to keep going.  Due to the weak links connecting participants to
the chosen group, the rate of dropouts is high, and its members tend to give up
in the face of the first adversities~\citep{jefferson}. As a result, many
projects may fail due to the insufficient number of contributors.

One way of helping newcomers in those communities is through their involvement
in study groups. The assistance offered by study groups can decrease the
learning curve associated with entering a FLOSS project and also keeping the
individual involved. Notwithstanding many advantages for academic teaching and
professional training, FLOSS development groups are not commonly found in
Brazilian universities.

With this concern, this work looks into the role played by a university group
for engaging students in FLOSS development and improving the member's retention
of these communities. We examined the case of FLUSP (FLOSS at
USP)\footnote{\url{https://flusp.ime.usp.br}}, a group of graduate and
undergraduate students at the University of São Paulo (USP) that aims to
contribute to FLOSS projects. Its members have sent contributions to several
projects, such as to the Linux kernel, Git, and GCC, as well as having
participated in the Google Summer of Code initiative and taking part in FLOSS
conferences.

To conduct this investigation, we applied qualitative research methods,
engaging in participant and non-participant observation of its members, two
study methods often used in social research. We paid closer attention to the
contributions of its seven most active members, in addition to administering
them a questionnaire related to their participation in the group. We studied
the methods of engagement used by this group, such as pair programming,
internal reviewing, and blog posting, and analyzed their efficacy. We also
examined the patches sent to FLUSP's internal mailing list, keeping track of
which ones were sent to the community's official list and, among those, the
ones who were accepted and merged into the project's source code. We
categorized them according to the author, contribution type, and whether they
were co-developed or not.

Besides that, we also began participating in its activities from January 2019
to November of the same year. We took part in FLUSP's weekly occurrences during
this period, which included event planning, bureaucratic issues, and
participation in two conferences. Immersion on the development process of FLOSS
software projects happened through the sending of contributions as a community
developer. As a consequence, we sent ten patches through the course of those
ten months to one of its most active projects, the subsystem of the Linux
kernel known as Industrial Input/Output
(IIO)\footnote{\url{https://01.org/linuxgraphics/gfx-docs/drm/driver-api/iio/index.html}}.
The IIO subsystem was created in 2009 by Jonathan Cameron and aggregates
drivers to devices that perform analog to digital and digital to analog
conversions, such as pressure sensors, temperature sensors, accelerometers, and
magnetometers. We decided to select it as the focus of this study since it
received the most contributions from the FLUSP group.

We have also sent a questionnaire to five of the seven studied subjects related
to their experience at FLUSP and regarding the FLOSS projects to which they
contribute. One of the participants did not answer the questionnaire due to
having participated in the elaboration of the questions. Afterward, we analyzed
the gathered data, inferring whether the group is fulfilling its role of
bringing newcomers to the FLOSS community and stimulating their permanence
there. We used the results to investigate whether this group is indeed
fulfilling its role of bringing contributors to those communities and
increasing their rates of permanence. The questions they had to answer can be
found in Appendix~\ref{cap:appe}.

Our results indicated that FLUSP fulfilled its role of helping the introduction
of its members in the FLOSS community. Its philosophy of knowledge sharing,
internal review, and pair programming served to mitigate the initial
difficulties new contributors often find when they begin to navigate a FLOSS
project.

%% -------------------------------------------------------------------------- %%
\section{Research Design}

In this work, we used qualitative research strategies for data collection and
interpretation: the case study and participant and non-participant observation.
Qualitative methods are research techniques for collecting and analyzing data
focused on the comprehension of the reasons and motivations behind a certain
occurrence~\citep{seaman1999qualitative}. To better understand the dynamics
involved in the kind of voluntary contribution that takes place in FLOSS
projects, we chose the FLUSP group to be the subject of our case study.

A case study is a qualitative research method that aims to provide the
necessary tools for a researcher to gain an understanding of complex events
such as the roles and rules involved in the dynamics of a specific community
through investigating its particular instances. It is with the help of the
evidence obtained through a case study that models developed for a theory can
be tested to see if they correspond to reality.

We methodologically study the participation of six of FLUSP members to the IIO
subsystem, in addition to having also contributed to it ourselves through the
sending of 10 patches. The importance of the participant observation can be seen
in the gathered knowledge the researcher experiences through immersion in the
phenomenon at hand and is thus able to gain further insight. In the same way, it
is also valuable to engage in non-participant observation to try to understand
the parties' actions without external interference.

The FLUSP mailing list received a total of 221 patches between 4th October 2018
and 25th October 2019. The participants were chosen based on their frequency of
contribution and permanence in the group. To make the data analysis possible, we
subscribed to the FLUSP internal mailing list and inspected all the patches sent
through it since its inception. After that, we crossed the patches against those
sent to the IIO mailing list. We organized the data gathered in a spreadsheet
arranged by title; date sent; main author; whether it was co-developed or not;
co-authors (if any); patch type; if it was sent to IIO; if it was accepted in
IIO; and links to the patch in FLUSP and IIO mailing lists.

When two or more developers write a patch, it receives in its description a tag
known as ``co-developed by''. We separated the patches between those that were
co-developed and not to study the frequency they are done and what drives
contributors to create patches together.

We observed that between different versions of a patchset, the number of
patches in it could vary. To investigate the number of unique patches sent by
each programmer, we considered the number of patches of the final version. The
final version of a patch set is the one that was sent to the IIO mailing list,
independently of it being accepted and merged to the maintainer's repository or
not.

\section{Organization of the capstone project}

In Chapter 2, we present a conceptualization of the terms that are used
throughout this work, in addition to furnishing a background on FLOSS history
and, especially, of the Linux kernel. We also report the main events that
happened in FLUSP's short existence and a brief account of USP's involvement in
the FLOSS world.

In Chapter 3, we probe more thoroughly into FLUSP's internal structure,
describing the tools of which the group make use and how they are used in the
training process of its members. Besides that, we provide a brief description
of the participants involved in the study, along with exposing the results we
observed in our research. This includes the examination of the patches sent by
the group's members, as well as an analysis of the answers given by the studied
subjects to the questionnaire we created.

Finally, in Chapter 4, we detail the conclusions we were able to observe
through our research and give motivation to the production of further works on
the subject.
