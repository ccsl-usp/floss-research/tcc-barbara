\documentclass{tikzposter}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{graphicx}
\usepackage{adjustbox}
\usepackage{enumitem}
\usepackage[backend=biber,style=numeric]{biblatex}
\usepackage{emory-theme}

\usepackage{mwe} % for placeholder images

\addbibresource{refs.bib}

% set theme parameters
\tikzposterlatexaffectionproofoff
\usetheme{EmoryTheme}
\usecolorstyle{EmoryStyle}

\title{\parbox{\linewidth}{\centering How to contribute to the Linux kernel project:\protect\\ the case of the FLUSP group}}
\author{Bárbara de Castro Fernandes\\ Supervisor: Prof. Dr. Paulo Meirelles\\ Co-supervisor: Melissa Wen}
\titlegraphic{\includegraphics[width=0.11\textwidth]{logo.png}}
\institute{Instituto de Matemática e Estatística - Universidade de São Paulo}

% begin document
\begin{document}
\maketitle
\centering
\begin{columns}
    \column{0.5}
    \block{Introduction}{
Open-source software communities depend on a continuous stream of contributors to keep on going. Due to the weak links connecting participants to the chosen community, the rate of dropouts is high, and its members tend to give up in the face of the first adversities. As a result, many projects have failed due to insufficient volunteer participation.\\

One of the existing ways of helping newcomers in those communities is through the involvement in study groups, which can decrease the learning curve associated with entering an open-source project. This work looks into the role of how those groups can encourage the software development of such projects and thus improve their member retention.\\

FLUSP, a portmanteau of FLOSS at USP, is "a group of graduate and undergraduate students at the University of São Paulo (USP) that aims to contribute to free software (Free/Libre/Open Source Software - FLOSS) projects."\cite{flusp} Its members have sent contributions to several open-source projects, such as to the Linux kernel, Git, and GCC, as well as having participated in the Google Summer of Code initiative and taking part in FLOSS conferences.
}

\block{Methodology}{
We will examine the case study of the FLUSP group through qualitative research methods, engaging in participant and non-participant observation of its members. We will pay closer attention to the contributions of its seven most active members, in addition to administering them a questionnaire related to their participation in the group. Afterward, we will analyze the gathered data, inferring whether the group is fulfilling its paper of bringing newcomers to the open-source community and stimulating their permanence there.\\

We studied the methods of engagement used by this group, such as pair programming, internal reviewing, and blog posting, and analyzed their efficacy. We also examined the patches sent to FLUSP's internal mailing list, keeping track of which ones were sent to the community's official list and, among those, the ones who were accepted and merged into the project's source code. I categorized them according to the author, contribution type, and whether they were co-developed or not.\\

Besides that, I also began participating in its activities from January 2019 to November of the same year. I took part in FLUSP's weekly occurrences during this period, which included event planning, bureaucratic issues, and participation in two conferences. Immersion on the development process of open-source software projects happened through the sending of contributions as a community developer. As a consequence, I sent 10 patches through the course of those ten months to one of its most active projects, the subsystem of the Linux kernel known as Industrial Input/Output (IIO)\cite{iio}.\\

The IIO subsystem aggregates drivers to devices that perform analog to digital and digital to analog conversions, such as pressure sensors, temperature sensors, accelerometers, and magnetometers. We decided to select it as the focus of this study since it was the project that received the most contributions from the chosen study group, with almost all of its members having participated in it at some point.
}

\block{Results}{
Of the people studied, Marcelo Schmitt was the one who sent most patches, with 19 patches as the main author and none as the secondary author, totalizing 16.5\% of the patches sent as the main author. The second person with most patches was Renato Geh, with 18 patches sent as the main author and none sent as the secondary author. This corresponds to 15.7\% of all the patches sent as the main author. The third person with most patches is Matheus Tavares, with 12 patches sent as the main author and two sent as the secondary author, which is equal to 10.4\% of the patches sent as the main author and 5.4\% of the patches sent as the secondary author. The fourth and fifth people with most patches were Lucas Oshiro and me, the former with 5 patches sent as the main author and 5 patches sent as the secondary author (corresponding, respectively, to 4.3\% of the patches sent as the main author and 13.5\% of the patches sent as the secondary author) and the latter with 10 patches sent as the main author and none as the secondary author (corresponding to 8.7\% of all the patches sent as the main author). The sixth person with most patches was Melissa Wen, who sent six patches as the main author and two as the secondary author, which corresponds, respectively, to 5.2\% and 5.4\% of the patches sent as main and as secondary author. The seventh person with most patches was Giuliano Belinassi, who sent four patches as the main author and two patches as the secondary author. This corresponds, respectively, to 3.5\% and 5.4\% of the patches sent as the main author and secondary author. The other participants sent in a total of 41 patches as the primary author (corresponding to 35.7\% of the total) and 26 patches as the secondary author (corresponding to 70.3\% of the total).
}
  
\column{0.5}
\block{}{
It is possible to see that, of the seven people studied, all of them had a code style patch as their first one. A code style patch is one that aims to correct the writing guidelines used during the composition of a program's source-code. This result is according to the expected, because patches of this kind are considered the most simple ones and, as such, are an acceptable way of familiarizing new contributors with the dynamics and contribution rules existing in the community.\\

We also notice that the more experienced contributors possessed fewer patches made together. Creating patches along with other programmers can be seen as a way of transmitting part of one's experience to the other. Therefore, it is to be expected that the less experienced contributors had more patches done in groups.\\

Additionally, it is expected that more experienced programmers have a more considerable amount of accepted patches among the ones sent to the IIO mailing list, and this scenario is precisely what happened within the collected data. We also noticed that more experienced contributors had a more significant proportion of patches sent to the IIO. Inexperience usually entails making more mistakes, so it's natural that the programmers that are in the community for a shorter amount of time have fewer patches sent to the IIO than their counterparties.\\

There were sent to FLUSP's mailing list, a total of 277 patches. If we were to count only the final version of the patches, this number drops to 115 - meaning that 41.5\% of the patches sent to this list were composed of final versions.
}

\block{Questionnaire Analysis}{
According to the questionnaire answers, the participants gave the following reasons for beginning participation in the group: giving and receiving help to contribute to free software, developing research that benefits society, and learning how to deal with a large codebase.\\

Three of the respondents stated they always put FLUSP's mailing list in copy when sending contributions. They justified it by saying this strengthens FLUSP as a group and increases its visibility in the FLOSS community. Another respondent stated he used to do this in the beginning, but that over time, he sometimes forgot to do it. The other person said he didn't put the list in copy because he is not actively contributing to the kernel and is currently the mentor in another project. As such, he is the one who reviews the patches related to this project.\\

The main difficulties pointed by the interviewees related to their engagement in a FLOSS project are: not knowing where to begin or what to do, difficulty finding documentation, and trouble understanding the source code and how it was organized. The way those problems were mitigated varied, with two members saying they first recurred to internet searches to try to find the solution, one member saying his first option was the project's IRC channel, another one saying he first recurred to FLUSP and the other that he first recurred to tutorials and documentation. Reading the community's mailing list, and asking questions to the project's maintainers were different ways those difficulties were solved.\\

All of the respondents agreed that FLUSP had helped them become FLOSS contributors. They credited the group as having rendered possible their insertion in those communities and for teaching them how to interact with its members.\\
}
 
\block{Conclusions}{
By providing a support group to its members, FLUSP decreased the learning curve related to the ingress in a new project and the learning of the contribution process in the free software community. Once the group was already established, its participants were able to exercise a collaborative practice during its weekly activities and reunions. FLUSP was also able to provide its most experienced members with an interesting environment where knowledge sharing was possible. This can be associated with a lower dropout rate and higher permanence in the community.\\

This work can provide ground to the premise that study groups can be used as tools to promote the free software initiative and improve retention of its members. Further studies about the subject are encouraged as a way to better ground these findings.
}
    
\block{References}{
    \begin{footnotesize}
    \begin{center}\mbox{}\vspace{-\baselineskip}
    \printbibliography[heading=none]
    \end{center}
    \end{footnotesize}
}
\end{columns}
\end{document}