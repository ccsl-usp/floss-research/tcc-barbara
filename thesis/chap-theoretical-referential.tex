%% -------------------------------------------------------------------------- %%
\chapter{Background}
\label{chap:theoretical-referential}

In this chapter, we introduce a brief account of the Linux kernel history and
its organization, and discuss the GNU Project and the concept of FLOSS, besides
describing how the Linux code is organized and how the contribution flow to its
subsystems work. Also, we present the history of GNU/Linux operating system in
the University of São Paulo and the creation of the FLOSS at USP group.

\section{The Linux Kernel}

Linux is an operating system kernel created by Linus Torvalds, a software
engineer from the University of Helsinki. Initially designed for i386 machines
and as a
hobby\footnote{\url{https://groups.google.com/forum/\#!topic/comp.os.minix/dlNtH7RRrGA}},
it found inspiration in MINIX~\citep{tanenbaum2011operating}, an operational
system created by Andrew Tanenbaum for educational purposes in 1987.

There are two main kinds of kernels, the microkernel and the monolithic kernel.
In the microkernel, this part of the operating system has the minimum amount of
software necessary to make it runnable. Besides, the user services and kernel
services are kept in different address spaces. The MINIX is an example of such
kind of kernel.

Also known as ``The Big Mess''~\citep{tanenbaum2011operating}, in the
monolithic kernel the entire operating system works in kernel space. And it is
not uncommon to have tasks like device drivers, file system and memory
management being performed by it. We can cite Linux as a kind of monolithic
kernel.  The duality of monolithic kernel and microkernel gave rise to many
feuds, including one between Tanenbaum and Torvalds. Details about this
discussion can be seen in the Usenet thread created by Tanenbaum in
comp.os.minix on January, 29th, 1992~\citep{dibona1999open}.

Soon Torvalds' project grew with the help of the GNU Project and was released
under the GNU General Public License as the GNU/Linux operating system. Richard
Stallman started the GNU Project in 1983 to replace all the most critical
software in the computer by a FLOSS alternative. In the vision of Stallman,
every software should grant its users four freedoms: the freedom to use, study,
share and improve
it\footnote{\url{https://fsfe.org/freesoftware/basics/4freedoms.en.html}}.
Those freedoms form the basis of the free software movement, and the GNU
General Public License exists to guarantee all software released under this
license have them respected.

Since the source code of the Linux kernel was first released to the world in
1991, GNU/Linux has grown to become one of the most famous operating systems
that ever existed. It is now considered the most successful example of a FLOSS
project. Nowadays, all the supercomputers in the TOP500 list use it as their
operational system\footnote{\url{https://www.top500.org/}}.

The GNU/Linux operating system can be divided into four main layers: the
hardware, the kernel, the shell and the applications. The hardware consists of
all the physical components of the computer. The kernel is the core component
of any operational system, providing communication with the hardware and
handling the system tasks. The shell operates as an interface that takes
commands directly from the user and executes kernel commands. The applications
are the programs that supply most of the functions of the computer. In this
work, we focus on the kernel layer.

\subsection{The Linux Development}

Some initiatives both from industry and academia have studied FLOSS development
characteristics~\citep{raymond1999cathedral, fitzgerald2006transformation}. One
of the first essays to broach this subject was Eric S. Raymond's The Cathedral
and the Bazaar. In this seminal work, Raymond exposed the experience he gained
by managing a project of this kind, the \textit{fetchmail}.

Raymond exemplified the bazaar model by the style of development employed by
the Linux project. This model is characterized as bordering on the
``promiscuous'' side. It is based on early and often releases and delegation of
everything possible. This goes directly against the Cathedral style of
development, that perpetuates that the final user must see the least amount of
bugs as possible in the code. The bazaar style rationale is exemplified by a
claim the author called Linus's Law: ``given enough eyeballs; all bugs are
shallow.''

The Linux kernel functions can be divided into the following five
parts~\citep{corbet2005linux}:

\begin{itemize}
\item Process management: a computer process is a program in
	execution. The process management of an operating
	system concerns process creation and termination, as
	well as the scheduling algorithms employed to share the
	CPU usage;

\item Memory management: most computers follow a memory hierarchy, having a
	small amount of fast, expensive, volatile memory (Random Access Memory,
	or RAM), and a larger quantity of slow, cheaper, non-volatile
	memory (Read-Only Memory, or ROM). Memory management pertains
	the part of the operating system that deals with this
	hierarchy, in addition to memory allocation and deallocation;

\item Filesystems: the filesystem is involved in information storage and
	retrieval. Linux follows the Unix methodology that asserts that almost
	anything can be treated as a file;

\item Device control: a device driver is the part of an operating system code
	involved in communication with the hardware. A driver provides
	device-specific code to perform input and output operations;

\item Networking: part of the operating system that controls the delivery and
collection of packets through a communication network. It also performs packet
routing and addresses translation.  \end{itemize}

Since the kernel has long stopped being a project able to be managed by a
single person, it now works with a chain of trust model. Most subsystems have a
maintainer, who is the person responsible for the patches that go through its
repository. Linux subsystems are all managed by volunteers, who have the duty
of ensuring the code quality. Each maintainer has its version of the kernel
source tree and uses version-control tools, such as Git, to trace information
about the patches contained on it.

All the subsystems have a staging directory, where the code for its drivers and
filesystems stay until they are well developed enough to become part of the
kernel source tree. Greg Kroah-Hartman currently maintains the staging tree.

Periodically, Torvalds' source tree begins accepting pull requests. When that
happens, the maintainers ask Torvalds to pull the patches they have selected
from their version of the source tree, thus becoming part of the mainline
branch.

One thing that facilitates the merging of their work is the kernel modularized
structure. Modules make it possible to extend the functionalities provided by
the kernel in runtime. They render more flexibility to the system, allowing the
dynamic linking and unlinking of pieces of code while the Linux is running.

Most of the code development done in Linux is made through mailing lists. In
this work, our focus is on the Industrial Input/Output (IIO) subsystem, which
is part of the device control portion of the kernel. The IIO aims to provide
support for devices that perform either an analog-to-digital conversion,
digital-to-analog, or both. It is currently maintained by Jonathan Cameron,
from Huawei, and has been since its inception in 2009.

Cameron started working on an academic project called SESAME, in which athletes
were equipped with movement sensors on their bodies. There were not many
platforms out there to do this.  He asked on the Linux kernel mailing list and
concluded that he would have to make something new. That is how he started
planning the IIO subsystem.

\subsection{The Contribution Workflow}

To begin contributing, a contributor must subscribe to the subsystem mailing
list, besides making a copy of the subsystem source code. It is also necessary
to set the development environment and to create a virtual machine to test the
changes made to the code without crashing the whole operational system the user
is using in the code development.

The entire contribution process begins by finding an issue in which to work.
This process can be done by accompanying the subsystem mailing list, looking
for the issues page in the project Wikipedia, or asking directly to a
maintainer.

Once an issue has been selected, the contributor must locate the sections of
the code that must be modified or where a new code must be added to fix the
problem at hand. Each patch must be composed of small pieces of code and must
address one issue at a time. It is common practice to break a series of
modifications in what is called a patch set. A patch set is nothing more than a
grouping of modifications, all related to the same problem.

After the code has been modified, the changes must be tested. The testing part
consists of compiling the source code to see if it breaks the code and running
the existing test files related to the sections of code modified in the patch.

Through the help of a contribution group, the created patch can be sent to an
internal mailing list. In this mailing list, the modifications are again tested
by other contributors to find issues the patch developer might have potentially
missed. After those issues are dealt with by the developer, the patch is again
sent to the internal mailing list until no other issues are found. When this
happens, the patch can be sent to the official mailing list.

\begin{lstlisting}[numbers=none, label=lst:commit, caption={The format a commit
message should have}, captionpos=b]
<subsystem>: <what changed>
<blank line>
<why this change was made>
<blank line>
<footer>
\end{lstlisting}

The sending of patches is managed with the help of another of Torvalds'
creation, the version-control tool named Git. In Git, each patch roughly
corresponds to one commit. Every commit must have a message describing what the
patch does. The first line of the message is the title, which briefly describes
what the proposed modifications do, and the last lines contain the footer. It is
in the footer that information about the people directly involved in its
development is placed. The format a commit message can be seen in
Listing~\ref{lst:commit}\footnote{\url{https://docs.flatcar-linux.org/container-linux-config-transpiler/contributing/}}.

\begin{figure}[ht] \centering
\includegraphics[width=0.9\linewidth]{contribution_flow.png}
\caption{Contribution workflow in a Linux subsystem}
\label{fig:contribution_flow}
\end{figure}

After the code is sent to the official mailing list, one of the maintainers is
in charge of testing the code. If any issue is found on the code, the developer
must apply the suggested modifications to have their patch accepted. If no
issue is found, the maintainer accepts the patch and have its code merged to
the kernel source tree version of their subsystem. In this way, the patch is
added to the next version of the kernel released.
We illustrate the whole contribution process in
Figure~\ref{fig:contribution_flow}.


\section{FLOSS at USP Group}

The Institute of Mathematics and Statistics of the University of São Paulo
(IME-USP) has a history with FLOSS since the installation of the GNU/Linux
operational system by Prof. Marco Dimas Gubitoso, in 1993. Today students at
the institute can benefit from the usage of the ``Rede Linux '', created in
1998 to provide them with the necessary computational resources. The university
involvement with FLOSS does not end there, though. In 2006, FAPESP and the
rectory released money for the construction of the FLOSS Competence Center
(CCSL). Two years afterwards, its statute was written, and this legally marked
the creation of the CCSL.

In this scenario, FLUSP acted as an empowerment tool that USP students could
use to contribute to complex FLOSS projects, such as the Linux kernel.  The
idea of creating FLUSP raised during the subject of Extreme Programming Lab.
Also known as LabXP, this discipline aims to provide students with an
opportunity to engage in a software project and make use of the Agile
development methodology. Four of the LabXP students went on to establish FLUSP.

During the subject, a Computer Science master student from IME-USP, strived to
transmit all the knowledge he had about FLOSS, the Agile methodology and the
Linux kernel. It only took a month for the students to send their first
patches, and by the second month, they did not need his help anymore. Prompted
by him, they started writing blog posts, since this was a huge differential for
the ones who wanted to apply to GSoC.

In December of 2018 began the race for the members who applied to this program.
As a result, three FLUSP members had their proposals accepted and went on to
contribute to GCC, the IIO subsystem and Git. It was also in this month, on the
10th, that FLUSP officialization as an extension group took place.

FLUSP currently has around six active members. It has also study groups
dedicated to Debian, GCC, Git and the Linux kernel. It holds weekly meetings in
which its members gather to work on these projects.

Each of the group projects is conducted as independent entities. The
participants have the prerogative concerning how the subgroup is managed. They
also define how and when its meetings are take place.

FLUSP has three main communication channels: a mailing list, an IRC channel and
a Telegram group. There is also a bot that performs integration between its IRC
channel and Telegram group, allowing the messages sent in one of them to be
available in the other.

FLUSP has an internal revision system in which members of the group review each
other patches before sending them to a FLOSS project (i.e., IIO mailing list).
This approach helps avoiding the sending of patches with minor mistakes and it
also teaches its members to review patches and become more acquainted with the
subsystem code.

Moreover, in its short story, FLUSP has already organized several FLOSS events:
the install fest 2019 at IME-USP, a Git workshop, and a marathon of
collaborations to the kernel IIO subsystem (KernelDevDay). FLUSP also supports
part of linuxdev-br 2019 edition. In particular, the KernelDevDay (KDD)
gathered 21 people and 16 patches were sent to the IIO mailing list.
